import React, { Component } from 'react';
import './footer.css';

class Footer extends Component {
  render() {
    return (
      <footer className="page-footer font-small blue pt-4 mt-4">
        <div className="container-fluid text-center text-md-left">
          <div className="row">
            <div className="col-md-6 mt-md-0 mt-3 footer-con-container">
              <h5 className="text-uppercase">Kontakt</h5>
              <p>blah blah</p>
            </div>

            <hr className="clearfix w-100 d-md-none pb-3" />

            <div className="col-md-6 mb-md-0 mb-3 footer-con-container">
              <h5 className="text-uppercase">Links</h5>

              <ul className="list-unstyled">
                <li>
                  <a href="#!">Link 1</a>
                </li>
                <li>
                  <a href="#!">Link 2</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </footer>
    );
  }
}

export default Footer;
