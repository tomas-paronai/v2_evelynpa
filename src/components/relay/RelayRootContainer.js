import React, { Component } from 'react';
import { QueryRenderer } from 'react-relay';
import Environment from '../../api/relay';

type Props = {
  variables?: Object,
  query: Function,
  onRenderLoading?: () => ?React.Node,
  onRenderError?: (error: Error) => ?React.Node,
  onRenderProps?: (props: ?Object) => ?React.Node
};

class RelayContainer extends Component<Props> {
  render() {
    return (
      <QueryRenderer
        environment={Environment}
        variables={this.props.variables}
        query={this.props.query}
        render={({ error, props }) => {
          if (error) {
            return this.props.onRenderError ? this.props.onRenderError(error) : null;
          } else if (props) {
            return this.props.onRenderProps ? this.props.onRenderProps(props) : null;
          }
          return this.props.onRenderLoading ? this.props.onRenderLoading() : null;
        }}
      />
    );
  }
}

export default RelayContainer;
