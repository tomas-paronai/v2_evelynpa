import { graphql } from 'react-relay';

export default graphql`
  fragment contentItemFragment_content on Content {
    _id
    name
    parent {
      _id
    }
    picture {
      thumbnail {
        name
        url
      }
    }
  }
`;
