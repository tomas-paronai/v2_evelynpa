/**
 * @flow
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteFragment } from 'relay-runtime';
import type { FragmentReference } from "relay-runtime";
declare export opaque type contentItemFragment_content$ref: FragmentReference;
export type contentItemFragment_content = {|
  +_id: string,
  +name: ?string,
  +parent: ?{|
    +_id: string
  |},
  +picture: ?{|
    +thumbnail: ?{|
      +name: ?string,
      +url: ?string,
    |}
  |},
  +$refType: contentItemFragment_content$ref,
|};
*/


const node/*: ConcreteFragment*/ = (function(){
var v0 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "_id",
  "args": null,
  "storageKey": null
},
v1 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "name",
  "args": null,
  "storageKey": null
};
return {
  "kind": "Fragment",
  "name": "contentItemFragment_content",
  "type": "Content",
  "metadata": null,
  "argumentDefinitions": [],
  "selections": [
    v0,
    v1,
    {
      "kind": "LinkedField",
      "alias": null,
      "name": "parent",
      "storageKey": null,
      "args": null,
      "concreteType": "Content",
      "plural": false,
      "selections": [
        v0
      ]
    },
    {
      "kind": "LinkedField",
      "alias": null,
      "name": "picture",
      "storageKey": null,
      "args": null,
      "concreteType": "Images",
      "plural": false,
      "selections": [
        {
          "kind": "LinkedField",
          "alias": null,
          "name": "thumbnail",
          "storageKey": null,
          "args": null,
          "concreteType": "Picture",
          "plural": false,
          "selections": [
            v1,
            {
              "kind": "ScalarField",
              "alias": null,
              "name": "url",
              "args": null,
              "storageKey": null
            }
          ]
        }
      ]
    }
  ]
};
})();
// prettier-ignore
(node/*: any*/).hash = '7182fa67fd5793c95ba175262d8857b0';
module.exports = node;
