import React, { Component } from 'react';
import { createFragmentContainer } from 'react-relay';
import { contentItemFragment } from './queries';

import type { Media } from './types';

type Props = {
  content: ?Object,
  renderContainer?: (data: Media) => ?React.Node
};

class ContentItemFragment extends Component<Props> {
  render() {
    return this.props.renderContainer ? this.props.renderContainer(this.props.content) : null;
  }
}

export default createFragmentContainer(ContentItemFragment, contentItemFragment);
