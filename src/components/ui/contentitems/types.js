export type Picture = {
  thumbnail: string,
  medium: string,
  original: string
};

export type Media = {
  _id: string,
  name: ?string,
  description: ?string,
  likes: ?boolean,
  isCol: ?boolean,
  parent: ?Media,
  picture: ?Picture
};
