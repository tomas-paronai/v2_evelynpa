import React, { Component } from 'react';

import type { Media } from '../types';

type Props = {
  item: Media
};

class MediaItem extends Component<Props> {
  render() {
    return (
      <div className="col-md" style={{ marginTop: '10px' }}>
        <div>
          <img
            src={this.props.item.picture.thumbnail.url}
            alt={this.props.item.picture.thumbnail.name}
          />
        </div>
      </div>
    );
  }
}

export default MediaItem;
