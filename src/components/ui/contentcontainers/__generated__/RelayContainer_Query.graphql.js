/**
 * @flow
 * @relayHash b7a4be17bbcd911fbcb8eedce6865179
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type RelayContainer_QueryVariables = {|
  ref: string
|};
export type RelayContainer_QueryResponse = {|
  +content: ?{|
    +name: ?string
  |}
|};
*/


/*
query RelayContainer_Query(
  $ref: String!
) {
  content(ref: $ref) {
    name
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "ref",
    "type": "String!",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "LinkedField",
    "alias": null,
    "name": "content",
    "storageKey": null,
    "args": [
      {
        "kind": "Variable",
        "name": "ref",
        "variableName": "ref",
        "type": "String!"
      }
    ],
    "concreteType": "Content",
    "plural": false,
    "selections": [
      {
        "kind": "ScalarField",
        "alias": null,
        "name": "name",
        "args": null,
        "storageKey": null
      }
    ]
  }
];
return {
  "kind": "Request",
  "operationKind": "query",
  "name": "RelayContainer_Query",
  "id": null,
  "text": "query RelayContainer_Query(\n  $ref: String!\n) {\n  content(ref: $ref) {\n    name\n  }\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "RelayContainer_Query",
    "type": "Query",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": v1
  },
  "operation": {
    "kind": "Operation",
    "name": "RelayContainer_Query",
    "argumentDefinitions": v0,
    "selections": v1
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = 'bc33e0f19518f75e4ab45c1706ee0636';
module.exports = node;
