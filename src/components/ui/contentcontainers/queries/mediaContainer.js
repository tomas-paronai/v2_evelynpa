import { graphql } from 'react-relay';

export default graphql`
  fragment mediaContainerFragment_container on Content
    @argumentDefinitions(isCol: { type: "Boolean" }) {
    _id
    name
    media(isCol: $isCol) {
      ...contentItemFragment_content
    }
  }
`;
