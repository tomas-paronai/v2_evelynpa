/**
 * @flow
 * @relayHash b9b86549ecb61c4b4021949f6f5d5726
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
type mediaContainerFragment_container$ref = any;
export type contentRoot_QueryVariables = {|
  ref: string,
  isCol?: ?boolean,
|};
export type contentRoot_QueryResponse = {|
  +content: ?{|
    +$fragmentRefs: mediaContainerFragment_container$ref
  |}
|};
*/


/*
query contentRoot_Query(
  $ref: String!
  $isCol: Boolean
) {
  content(ref: $ref) {
    ...mediaContainerFragment_container_42wjl8
  }
}

fragment mediaContainerFragment_container_42wjl8 on Content {
  _id
  name
  media(isCol: $isCol) {
    ...contentItemFragment_content
  }
}

fragment contentItemFragment_content on Content {
  _id
  name
  parent {
    _id
  }
  picture {
    thumbnail {
      name
      url
    }
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "ref",
    "type": "String!",
    "defaultValue": null
  },
  {
    "kind": "LocalArgument",
    "name": "isCol",
    "type": "Boolean",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "ref",
    "variableName": "ref",
    "type": "String!"
  }
],
v2 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "_id",
  "args": null,
  "storageKey": null
},
v3 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "name",
  "args": null,
  "storageKey": null
};
return {
  "kind": "Request",
  "operationKind": "query",
  "name": "contentRoot_Query",
  "id": null,
  "text": "query contentRoot_Query(\n  $ref: String!\n  $isCol: Boolean\n) {\n  content(ref: $ref) {\n    ...mediaContainerFragment_container_42wjl8\n  }\n}\n\nfragment mediaContainerFragment_container_42wjl8 on Content {\n  _id\n  name\n  media(isCol: $isCol) {\n    ...contentItemFragment_content\n  }\n}\n\nfragment contentItemFragment_content on Content {\n  _id\n  name\n  parent {\n    _id\n  }\n  picture {\n    thumbnail {\n      name\n      url\n    }\n  }\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "contentRoot_Query",
    "type": "Query",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "content",
        "storageKey": null,
        "args": v1,
        "concreteType": "Content",
        "plural": false,
        "selections": [
          {
            "kind": "FragmentSpread",
            "name": "mediaContainerFragment_container",
            "args": [
              {
                "kind": "Variable",
                "name": "isCol",
                "variableName": "isCol",
                "type": null
              }
            ]
          }
        ]
      }
    ]
  },
  "operation": {
    "kind": "Operation",
    "name": "contentRoot_Query",
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "content",
        "storageKey": null,
        "args": v1,
        "concreteType": "Content",
        "plural": false,
        "selections": [
          v2,
          v3,
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "media",
            "storageKey": null,
            "args": [
              {
                "kind": "Variable",
                "name": "isCol",
                "variableName": "isCol",
                "type": "Boolean"
              }
            ],
            "concreteType": "Content",
            "plural": true,
            "selections": [
              v2,
              v3,
              {
                "kind": "LinkedField",
                "alias": null,
                "name": "parent",
                "storageKey": null,
                "args": null,
                "concreteType": "Content",
                "plural": false,
                "selections": [
                  v2
                ]
              },
              {
                "kind": "LinkedField",
                "alias": null,
                "name": "picture",
                "storageKey": null,
                "args": null,
                "concreteType": "Images",
                "plural": false,
                "selections": [
                  {
                    "kind": "LinkedField",
                    "alias": null,
                    "name": "thumbnail",
                    "storageKey": null,
                    "args": null,
                    "concreteType": "Picture",
                    "plural": false,
                    "selections": [
                      v3,
                      {
                        "kind": "ScalarField",
                        "alias": null,
                        "name": "url",
                        "args": null,
                        "storageKey": null
                      }
                    ]
                  }
                ]
              }
            ]
          }
        ]
      }
    ]
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = '3a95b98ed2b1f11ebf06720133db0c7e';
module.exports = node;
