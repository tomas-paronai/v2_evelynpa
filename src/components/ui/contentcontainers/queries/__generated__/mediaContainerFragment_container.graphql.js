/**
 * @flow
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteFragment } from 'relay-runtime';
type contentItemFragment_content$ref = any;
import type { FragmentReference } from "relay-runtime";
declare export opaque type mediaContainerFragment_container$ref: FragmentReference;
export type mediaContainerFragment_container = {|
  +_id: string,
  +name: ?string,
  +media: ?$ReadOnlyArray<?{|
    +$fragmentRefs: contentItemFragment_content$ref
  |}>,
  +$refType: mediaContainerFragment_container$ref,
|};
*/


const node/*: ConcreteFragment*/ = {
  "kind": "Fragment",
  "name": "mediaContainerFragment_container",
  "type": "Content",
  "metadata": null,
  "argumentDefinitions": [
    {
      "kind": "LocalArgument",
      "name": "isCol",
      "type": "Boolean",
      "defaultValue": null
    }
  ],
  "selections": [
    {
      "kind": "ScalarField",
      "alias": null,
      "name": "_id",
      "args": null,
      "storageKey": null
    },
    {
      "kind": "ScalarField",
      "alias": null,
      "name": "name",
      "args": null,
      "storageKey": null
    },
    {
      "kind": "LinkedField",
      "alias": null,
      "name": "media",
      "storageKey": null,
      "args": [
        {
          "kind": "Variable",
          "name": "isCol",
          "variableName": "isCol",
          "type": "Boolean"
        }
      ],
      "concreteType": "Content",
      "plural": true,
      "selections": [
        {
          "kind": "FragmentSpread",
          "name": "contentItemFragment_content",
          "args": null
        }
      ]
    }
  ]
};
// prettier-ignore
(node/*: any*/).hash = '730410a3e38e04002617fae9c2aa05fc';
module.exports = node;
