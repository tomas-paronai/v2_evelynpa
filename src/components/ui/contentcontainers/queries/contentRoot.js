import { graphql } from 'react-relay';

export default graphql`
  query contentRoot_Query($ref: String!, $isCol: Boolean) {
    content(ref: $ref) {
      ...mediaContainerFragment_container @arguments(isCol: $isCol)
    }
  }
`;
