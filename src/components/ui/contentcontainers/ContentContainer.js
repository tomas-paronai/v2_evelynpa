import React, { Component } from 'react';
import RelayContainer from '../../relay';
import { contentRootQuery } from './queries';
import MediaContainerFragment from './MediaContainerFragment';

type Props = {
  variables: Object,
  fragmentAlias?: string,
  renderContainer: (data: ?Object) => ?React.Node
};

class ContentContainer extends Component<Props> {
  static defaultProps = {
    fragmentAlias: 'content',
    renderContainer: data => {
      console.warn('No container render provided');
      console.log('Data > ', data);
      return null;
    }
  };
  render() {
    return (
      <RelayContainer
        query={contentRootQuery}
        variables={this.props.variables}
        onRenderProps={props => (
          <MediaContainerFragment
            container={props.content}
            onRenderContainer={this.props.renderContainer}
          />
        )}
      />
    );
  }
}

export default ContentContainer;
