import React, { Component } from 'react';
import { createFragmentContainer } from 'react-relay';
import { mediaContainerFragment } from './queries';
import type { Gallery } from './types';

type Props = {
  container: Object,
  onRenderContainer?: (data: Gallery) => ?React.Node
};

class MediaContainerFragment extends Component<Props> {
  render() {
    return this.props.onRenderContainer ? this.props.onRenderContainer(this.props.container) : null;
  }
}

export default createFragmentContainer(MediaContainerFragment, mediaContainerFragment);
