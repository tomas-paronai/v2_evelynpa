import type { Media } from '../contentitems';

export type Gallery = {
  _id: string,
  name: ?string,
  description: ?string,
  likes: ?boolean,
  media: ?Array<Media>
};
