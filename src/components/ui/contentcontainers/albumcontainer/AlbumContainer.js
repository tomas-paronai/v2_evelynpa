import React, { Component } from 'react';
import { ContentItemFragment, MediaItem } from '../../contentitems';
import './albumContainer.css';

import type { Gallery } from '../types';

type Props = {
  containerData: Gallery
};

class MediaContainer extends Component<Props> {
  render() {
    return (
      <div className="container-fluid">
        <div className="row">
          {this.props.containerData.media.map((item, index) => (
            <ContentItemFragment
              key={String(index)}
              content={item}
              renderContainer={data => <MediaItem item={data} />}
            />
          ))}
        </div>
      </div>
    );
  }
}

export default MediaContainer;
