import React, { Component } from 'react';
import {
  Navbar,
  Nav,
  NavItem,
  NavLink,
  Dropdown,
  DropdownToggle,
  DropdownItem,
  DropdownMenu
} from 'reactstrap';
import Responsive from 'react-responsive-decorator';
import './navigation.css';

type Props = {
  media: (options: Object, callback: () => void) => void
};

type State = {
  show: boolean,
  isMobile: boolean
};

@Responsive
class Navigation extends Component<Props, State> {
  constructor(props) {
    super(props);
    this.state = {
      show: false,
      isMobile: false
    };
  }

  componentDidMount() {
    this.props.media({ minWidth: 468 }, () => {
      this.setState({
        isMobile: false
      });
    });

    this.props.media({ maxWidth: 468 }, () => {
      this.setState({
        isMobile: true
      });
    });
  }

  _toggle = () => {
    this.setState({ show: !this.state.show });
  };

  render() {
    return (
      <Navbar dark className="navbar">
        {this.state.isMobile ? (
          <Dropdown isOpen={this.state.show} toggle={this._toggle}>
            <DropdownToggle className="button-toggle">
              <div className="hamburger-container">
                <div className="bar1" />
                <div className="bar2" />
                <div className="bar3" />
              </div>
            </DropdownToggle>
            <DropdownMenu className="dropdown-container">
              <DropdownItem>
                <a href="/home">Domov</a>
              </DropdownItem>
              <DropdownItem divider />
              <DropdownItem>
                <a href="/about">O nas</a>
              </DropdownItem>
            </DropdownMenu>
          </Dropdown>
        ) : (
          <Nav>
            <NavItem>
              <NavLink href="/home">Domov</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="/about">O nas</NavLink>
            </NavItem>
          </Nav>
        )}
      </Navbar>
    );
  }
}

export default Navigation;
