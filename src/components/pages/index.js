import Home from './home';
import About from './about';
import RouteContainer from './RouteContainer';

export default RouteContainer;
export { Home, About };
