import React, { Component } from 'react';
import { Router, Switch, Route, Redirect } from 'react-router-dom';
import createBrowserHistory from 'history/createBrowserHistory';
import Navigation from '../navigation';
import Footer from '../footer';
import Home from './home';
import About from './about';
import './rootContainer.css';

class RouterContainer extends Component {
  constructor(props) {
    super(props);
    this.history = createBrowserHistory();
  }

  render() {
    return (
      <Router history={this.history}>
        <div className="root-container">
          <Navigation />
          <Switch>
            <Route path="/about" component={About} />
            <Route path="/home" component={Home} />
            <Route exact path="/" render={() => <Redirect to="/home" />} />
          </Switch>
          <Footer />
        </div>
      </Router>
    );
  }
}

export default RouterContainer;
