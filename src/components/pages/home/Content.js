import React, { Component } from 'react';
import ContentContainer, { MediaContainer, AlbumContainer } from '../../ui/contentcontainers';

type Props = {
  match: Object
};

class Content extends Component<Props> {
  render() {
    const { match: { params: { galleryRef = 'PTcMC4EmieXMYoqd3hN8' } } } = this.props;
    return (
      <div>
        <ContentContainer
          variables={{ ref: galleryRef, isCol: false }}
          renderContainer={data => <MediaContainer containerData={data} />}
        />
        <ContentContainer
          variables={{ ref: galleryRef, isCol: true }}
          renderContainer={data => <AlbumContainer containerData={data} />}
        />
      </div>
    );
  }
}

export default Content;
