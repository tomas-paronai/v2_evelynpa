import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import Content from './Content';

class Home extends Component {
  render() {
    return <Route render={props => <Content {...props} />} path="/home/:galleryRef?" />;
  }
}

export default Home;
