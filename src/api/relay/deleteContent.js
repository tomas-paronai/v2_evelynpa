import { graphql, commitMutation } from 'react-relay';
import RelayEnvironment from './RelayEnvironment';

const mutation = graphql`
  mutation deleteContentMutation($ref: String!) {
    deleteContent(ref: $ref) {
      _id
    }
  }
`;

export const deleteContent = (ref: string, onCompleted?: () => void) =>
  commitMutation(RelayEnvironment, {
    mutation,
    variables: { ref },
    onCompleted
  });
