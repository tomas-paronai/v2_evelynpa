/**
 * @flow
 * @relayHash 60175f0e9a69e320a1e653e2ee3914d2
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type deleteContentMutationVariables = {|
  ref: string
|};
export type deleteContentMutationResponse = {|
  +deleteContent: ?{|
    +_id: string
  |}
|};
*/


/*
mutation deleteContentMutation(
  $ref: String!
) {
  deleteContent(ref: $ref) {
    _id
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "ref",
    "type": "String!",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "LinkedField",
    "alias": null,
    "name": "deleteContent",
    "storageKey": null,
    "args": [
      {
        "kind": "Variable",
        "name": "ref",
        "variableName": "ref",
        "type": "String!"
      }
    ],
    "concreteType": "Content",
    "plural": false,
    "selections": [
      {
        "kind": "ScalarField",
        "alias": null,
        "name": "_id",
        "args": null,
        "storageKey": null
      }
    ]
  }
];
return {
  "kind": "Request",
  "operationKind": "mutation",
  "name": "deleteContentMutation",
  "id": null,
  "text": "mutation deleteContentMutation(\n  $ref: String!\n) {\n  deleteContent(ref: $ref) {\n    _id\n  }\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "deleteContentMutation",
    "type": "Mutation",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": v1
  },
  "operation": {
    "kind": "Operation",
    "name": "deleteContentMutation",
    "argumentDefinitions": v0,
    "selections": v1
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = 'bab66c9e2f49c5b4c826a503be552d35';
module.exports = node;
