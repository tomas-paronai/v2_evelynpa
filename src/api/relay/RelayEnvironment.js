import { Environment, Network, RecordSource, Store } from 'relay-runtime';

/**
 * TODO
 * 1. Change image schema to have image name so we can find and delete in storage - DONE
 * 2a. Album and item creation separate - add collection field inside schema - DONE
 * 2b. Add checkbox to decide if creating album - DONE
 * 3. Multiple upload in case of album pictures
 * 4. Show picture name on hover & enable edit in admin mode
 * 5. Update with mutation on fly
 */

const fetchQuery = (operation, variables) => {
  const queryRequest = {
    method: 'POST',
    headers: {
      'content-type': 'application/json'
    }
  };

  queryRequest.body = JSON.stringify({
    query: operation.text,
    variables
  });
  return fetch('https://us-central1-evelynpa-43e6d.cloudfunctions.net/api/graphql', queryRequest)
    .then(response => response.json())
    .catch(err => console.warn(err));
};

const source = new RecordSource();
const store = new Store(source);
const network = Network.create(fetchQuery);
const handlerProvider = null;

const environment = new Environment({
  handlerProvider,
  network,
  store
});

export default environment;
