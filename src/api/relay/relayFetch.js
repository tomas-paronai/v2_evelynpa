import { fetchQuery } from 'relay-runtime';
import RelayEnvironment from './RelayEnvironment';

const relayFetch = (query, variables, cacheConfig) =>
  fetchQuery(RelayEnvironment, query, variables, cacheConfig);

export default (query, variables) => relayFetch(query, variables);
